# Definizione di una metrica #

## Una possibile definizione di distanza tra due ambiti ##

Ogni ambito rappresenta un'area, un insieme di scuole.

Gli ambiti non sono quindi puntiformi e la loro distanza non è definita in modo ovvio.

Si potrebbe essere tentati dalla possibilità di selezionare un singolo punto come rappresentativo di un ambito per calcolare le distanze a partire da quello. L'impressione è che questa idea porti ad una eccessiva semplificazione, non permettendo di tenere in considerazione l'effettiva estensione degli ambiti.

Altra tentazione potrebbe essere quella di considerare la distanza minima: se dovessimo dire quanto è distante Torino dalla Francia, misureremmo la strada più breve... Ne deriverebbe che la distanza tra aree confinanti è **zero**; questo da un lato non ci piace perché non ci aiuta a distinguere, tra tante aree confinanti, quale sia la più vicina. Dall'altro non è affatto convincente quando il confine tra due aree è posto su una barriera naturale, come il crinale di una montagna.

Una classica estensione del concetto di distanza a insiemi o aree è la [distanza di Hausdorff](https://it.wikipedia.org/wiki/Distanza_di_Hausdorff); questa sicuramente permetterebbe di definire una metrica nel senso matematico, ma potrebbe non fornire una corretta stima di quella che è percepibile come distanza tra due ambiti.

Abbiamo quindi pensato di definire la distanza tra due ambiti *A* e *B* come il massimo, sulle coppie di scuole, *a* in *A* e *b* in *B*, della distanza tra la scuola *a* e la scuola *b*.

Questa definizione, mettendo anche a punto i dettagli, permette di calcolare operativamente una distanza tra due ambiti, che gode della proprietà triangolare, ma non delle altre proprietà che ci si aspetterebbe da una distanza. Ad esempio prendendo un singolo ambito *A*, la distanza dell'ambito con se stesso non risulterebbe zero, ma il diametro (tra i due punti di maggior distanza tra loro) dell'ambito stesso.

## La distanza tra scuole ##

La distanza *in linea d'aria* non sembra significativa, con questa distanza si rischierebbe di sottostimare l'effettivo peso di uno spostamento tra scuole separate da barriere naturali. Serve quindi misurare la distanza su percorsi che collegano le scuole.

Anche questa distanza potrebbe non essere una metrica nel senso matematico, se misuriamo la distanza tra la scuola *a* e la scuola *b* con la lunghezza del percorso che collega la prima alla seconda, tale distanza potrebbe non essere simmetrica (la distanza da *a* a *b* potrebbe essere diversa dalla distanza da *b* ad *a*) a causa di porzioni del tragitto per i quali solo un senso di percorrenza è possibile.

Se invece della lunghezza si stima il tempo necessario per spostarsi da una scuola all'altra, alle possibili ragioni di asimmetria si aggiungono le differenze rilevanti di quota.

Pertanto, decidendo di utilizzare una distanza basata sulla percorrenza lungo strade, sarà opportuno calcolare entrambe le percorrenze, da *a* a *b* e viceversa, per poi considerare come distanza la somma (o la media) tra questi due valori. Oltre al vantaggio di ottenere un valore per definizione simmetrico, avremo anche dal punto di vista intuitivo un valore più rappresentativo, che terrà conto della peso sia dell'andare che del tornare.

## Le catene non sono simmetriche ##

Da sfatare l'idea che le catene di prossimità debbano essere simmetriche.

Si dice: se abbiamo in ordine *A*, *B*, *C* e *D*, allora dall'altra parte avremo *D*, *C*, *B* ed *A*. Ciò non è vero in generale, ma solo in casi molto particolari.

Supponiamo ad esempio di avere su una linea *B:A::C::D*. Partendo da *A*, potremmo dire che le distanze determinano proprio l'ordine descritto prima, quello alfabetico, ma partendo da *D* l'ordine non sarebbe quello inverso, *A* è posizionato tra *C* e *B*.

Se gli esempi si cercano in due dimensioni, la situazione diventa ancora più complicata e nemmeno scegliendo due "estremi" in insiemi di almeno quattro punti si avrà la garanzia che le catene siano simmetriche.