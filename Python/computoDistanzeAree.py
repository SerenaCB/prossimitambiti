#!/usr/bin/env python
# -*- coding: utf-8 -*-

##en
#    computoDistanzeAree, computes distances of sets of points
#    Copyright (C) 2016, 2017  Marco BODRATO
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##it
#    computoDistanzeAree, analizza le distanze tra insiemi di punti
#    Scritto nel 2016-2017 da Marco BODRATO, che ne detiene i diritti d'autore
#
#    Questo programma è software libero: può essere ridistribuito o modificato
#    nei termini della licenza "GNU Affero General Public License" come
#    pubblicata dalla Free Software Foundation; o la versione 3 della
#    licenza, o (a vostra scelta) qualunque versione successiva.
#
#    Questo programma è distribuito con l'auspicio che sarà utile,
#    ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
#    VENDIBILITÀ o ADEGUATEEZZA AD UN PARTICOLARE SCOPO. Si veda la
#    GNU Affero General Public License per ulteriori dettaglio.
#
#    Dovreste aver ricevuto una copia della GNU Affero General Public License
#    con a questo programma. Vedere altrimenti <http://www.gnu.org/licenses/>.
#

import csv
import locale
import requests
from math import sqrt, ceil

class Errore(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def nextprime (n):
    """Naively search for the smallest prime natural number >= n > 0"""
    if n < 3:
        return 2
    n |= 1
    d = 3
    while d*d <= n:
        if n%d == 0:
            d = 1
            n += 2
        d += 2
    return n

def checkdim (dim):
    """Verifica metodo di riempimento matrice con suddivisione in blocchi
    con un numero primo di punti."""
    matriceDistanze = bytearray(dim*dim);
    dimBlocco = nextprime (int(ceil(sqrt(dim))))
    chiamate = 0
    for i in range (0, dim, dimBlocco):
        v = range (i, min (dim, i+dimBlocco))
        if len (v) == 1:
            matriceDistanze [v[0]*(dim+1)] += 1
        else:
            chiamate += 1
            for j in v:
                for k in v:
                    if j != k:
                        matriceDistanze [j*dim+k] += 1
                    else:
                        matriceDistanze [j*dim+k] += 1
    for d in range (0, dimBlocco):
        for s in range (0, dimBlocco):
            v = [s]
            for i in range (1, dimBlocco):
                j = i * dimBlocco + (s + d * i) % dimBlocco
                if j < dim:
                    v.append (j)
            chiamate += 1
            for j in v:
                for k in v:
                    if j != k:
                        matriceDistanze [j*dim+k] += 1
    for i in matriceDistanze:
        if i != 1:
            return -1
    return chiamate

def distanza (p1, p2):

    """Distanza grossolana in chilometri tra i due punti p1 e p2, decente
    attorno al 45° parallelo. La funzione calcola una "distanza" in
    gradi poi rapportata ai 40000 km di circonferenza, sfruttando il
    fatto che sin(45°)^2 = cos(45°)^2 = 1/2...
    Per i due punti si ha p[0]=lon, p[1]=lat.
    """
    return sqrt((p1[0]-p2[0])**2/2+(p1[1]-p2[1])**2)*40000/360

#
# Lettura dati iniziali
#

fonteDati = 'Iniziali'
nomeFileDati = 'datiEsempio.csv'
separatoreDati = ';'
try:
    csvDatiIniziali = csv.DictReader (open (nomeFileDati), delimiter = separatoreDati)
    print ('Leggo i dati', fonteDati, 'dal file', nomeFileDati)
except:
    print ('Errore nella lettura del file', nomeFileDati, '.')
    raise

if not (('Area' in csvDatiIniziali.fieldnames) and ('Punto' in csvDatiIniziali.fieldnames) and ('Lat' in csvDatiIniziali.fieldnames) and ('Long' in csvDatiIniziali.fieldnames)):
    print ('I nomi attesi delle colonne (Area,Punto,Lat,Long) non sono stati trovati tra i nomi presenti nel file', nomeFileDati, 'che sono:', csvDatiIniziali.fieldnames)
    raise

locale.setlocale (locale.LC_ALL, '')

zone = {}
punti = {}

for rigaPunto in csvDatiIniziali:
    area = rigaPunto['Area']
    punto = rigaPunto['Punto']
    longitudine = locale.atof (rigaPunto['Long'])
    latitudine = locale.atof (rigaPunto['Lat'])
    if punto in punti:
        print ('Punto', punto, 'duplicato.')
    else:
        punti[punto] = (longitudine, latitudine)
    if not (area in zone):
        zone[area] = set ();
    zone[area] |= {punto};

puntiOrdinati = []
for area in zone:
    print (area, ':', len (zone[area]))
    for punto in zone[area]:
        print (punto, ':', punti[punto])
        puntiOrdinati.append (punto)

url = 'http://router.project-osrm.org/table/v1/driving/'
for punto in puntiOrdinati:
    url += repr(punti[punto][0])
    url += ','
    url += repr(punti[punto][1])
    url += ';'

url = url[:-1]

risposta = requests.get(url)
if risposta.status_code != 200:
    print ('Sito non disponibile o URL errato.')
    raise

try:
    risposta = risposta.json()
except:
    risposta = {}

if risposta.pop('code') != 'Ok':
    print ('Il sito non ha potuto fornire la risposta')
    raise

if risposta.pop('destinations') != risposta['sources']:
    print ('Destinazioni e partenze sono diverse')
    raise

for p, c in zip (risposta['sources'], puntiOrdinati):
    if distanza (punti[c], p['location']) > 0.5:
        print ('Per il punto', c, punti[c], 'il sito ha preso in cosiderazione le coordinate', p['location'], 'a circa', distanza (punti[c], p['location']), 'Km di distanza...')
