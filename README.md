# Calcolo delle catene di prossimità tra ambiti territoriali #

Parole e codice sul calcolo della prossimità di diversi ambiti territoriali.
Per ora, più che una indicazione sul contenuto di questa collezione, il presente documento contiene una traccia, alcuni punti da sviluppare.

## Appunti già presenti ##

* Sulla [definizione della distanza](DefinizioneDistanza.md);
* sullo [riempimento della matrice delle distanze](RiempimentoMatrice.md);
* dati sulle [autonomie scolastiche del Piemonte](Python/DatiScuole2016-17.csv);
* script per il [calcolo delle distanze in Python](Python/computoDistanzeAree.py).

## Discussione ##

* Definizione di una metrica, ragioni della definizione;
* possibilità di calcolo.
* Scelta degli strumenti.
* Scelta delle licenze d'uso per dati, codice, testi. Possibili scelte:
    * per il codice: GPL-v3+, [AGPL-v3+](LICENCE.md), EUPL;
    * per i testi: CC-BY-SA-v4, GFDL;
    * per i dati: CC-BY-v4, [ODbL](http://opendatacommons.org/licenses/odbl/), [IODLv2](http://www.dati.gov.it/iodl/2.0/).

## Dati ##

* Da dove prendere i dati, come controllarli e migliorarli;
* formato dei dati in ingresso.
* Analisi dei dati disponibili per il Piemonte.
* Dati in uscita, cosa serve, cosa altro può venirne fuori;
* formato dei dati in uscita.

## Codice ##

* Linguaggio?
    * Python, comodo, ma non da usare sulle postazioni dell'ufficio;
        * per usare i servizi HTTP e json: [Requests](http://docs.python-requests.org/)
        * [API di OSRM](http://project-osrm.org/docs/v5.5.2/api/#table-service);
        * leggere-scrivere csv: `import csv` dovrebbe funzionare su qualunque installazione.
    * JavaScript, potrebbe girare sui browser, senza installazioni di sorta...
* Codice per interrogare servizi di *routing*.
* Codice per calcolare distanze tra ambiti.