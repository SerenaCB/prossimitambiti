checkdim(dim)={
  M=matrix(dim,dim);
  c = 0;
  sqrtdim=nextprime(sqrtint(dim)+1-issquare(dim));
  forstep(i=1,dim,sqrtdim,c++;for(j=i,min(dim,i+sqrtdim-1),for(k=i,min(dim,i+sqrtdim-1),if(j!=k,M[j,k]++))));
  for(d=0,sqrtdim-1,
    for(s=1,sqrtdim,
      v=[s];
      for(i=1,sqrtdim-1,
        j=i*sqrtdim+1+(s-1+d*i)%sqrtdim;
        if(j<=dim,v=concat(v,j))
      );
      if (#v>1,c++);
      for(j=1,#v,
        for(k=1,#v,
          if(j!=k,M[v[j],v[k]]++)
        )
      )
    )
  );
  r=1;
  for(i=1,dim,for(j=1,dim,r*=(M[i,j]==(i!=j))));
  if(r,c)
}